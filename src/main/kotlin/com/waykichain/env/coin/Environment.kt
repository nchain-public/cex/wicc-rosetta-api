package com.waykichain.com.waykichain.env.coin

import com.waykichain.commons.util.BaseEnv

object Environment {
    object Environment {

        /**
         * config of wicc
         */
        @JvmField
        var WICC_HOST_IP = BaseEnv.env("WICC_HOST_IP", "10.0.0.31")
        @JvmField
        var WICC_HOST_PORT = BaseEnv.env("WICC_HOST_PORT", 6968)
        @JvmField
        var WICC_RPC_USERNAME = BaseEnv.env("WICC_RPC_USERNAME", "wayki")
        @JvmField
        var WICC_RPC_PASSWORD = BaseEnv.env("WICC_RPC_PASSWORD", "admin@123")

    }
}