package com.waykichain.controller

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.waykichain.api.ApiUtil
import com.waykichain.com.waykichain.bean.BuildTransactionBean
import com.waykichain.config.Constant
import com.waykichain.model.*
import com.waykichain.service.ChainXService
import com.waykichain.wallet.base.CoinType
import com.waykichain.wallet.base.HashReader
import com.waykichain.wallet.base.HashWriter
import com.waykichain.wallet.base.UCoinDest
import com.waykichain.wallet.base.params.WaykiMainNetParams.Companion.instance
import com.waykichain.wallet.base.params.WaykiTestNetParams
import com.waykichain.wallet.base.types.encodeInOldWay
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.tuple.Pair
import org.bitcoinj.core.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import org.springframework.web.context.request.NativeWebRequest
import java.lang.Error
import java.util.*
import java.util.concurrent.atomic.AtomicInteger
import javax.validation.Valid
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

@Controller
class ConstructionApiController @Autowired constructor(private val request: NativeWebRequest) {
    @Autowired
    lateinit var chainXService: ChainXService

    private val objectMapper = ObjectMapper()
    fun getRequest(): Optional<NativeWebRequest> {
        return Optional.ofNullable(request)
    }

    @ApiOperation(value = "Derive an Address from a PublicKey", nickname = "constructionDerive", notes = "Derive returns the network-specific address associated with a public key. Blockchains that require an on-chain action to create an account should not implement this method.", response = ConstructionDeriveResponse::class, tags = ["Construction"])
    @ApiResponses(value = [ApiResponse(code = 200, message = "Expected response to a valid request", response = ConstructionDeriveResponse::class), ApiResponse(code = 200, message = "unexpected error", response = Error::class)])
    @RequestMapping(value = ["/construction/derive"], produces = ["application/json"], consumes = ["application/json"], method = [RequestMethod.POST])
    fun constructionDerive(
            @ApiParam(value = "", required = true) @RequestBody constructionDeriveRequest: @Valid ConstructionDeriveRequest?): ResponseEntity<ConstructionDeriveResponse> {
        if (getRequest().isPresent) {
            for (mediaType in MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    val publicKey = constructionDeriveRequest!!.publicKey
                    if (CurveType.SECP256K1 == publicKey.curveType) {
                        val hexBytes = publicKey.hexBytes
                        val address = derivePubKey(hexBytes)
                        val response = ConstructionDeriveResponse()
                        response.address(address)
                        return ResponseEntity(response, HttpStatus.OK)
                    }
                    break
                }
            }
        }
        return ResponseEntity(HttpStatus.valueOf(200))
    }


    @ApiOperation(value = "Create a Request to Fetch Metadata", nickname = "constructionPreprocess", notes = "Preprocess is called prior to `/construction/payloads` to construct a request for any metadata that is needed for transaction construction given (i.e. account nonce). The request returned from this method will be used by the caller (in a different execution environment) to call the `/construction/metadata` endpoint.", response = ConstructionPreprocessResponse::class, tags = ["Construction"])
    @ApiResponses(value = [ApiResponse(code = 200, message = "Expected response to a valid request", response = ConstructionPreprocessResponse::class), ApiResponse(code = 200, message = "unexpected error", response = Error::class)])
    @RequestMapping(value = ["/construction/preprocess"], produces = ["application/json"], consumes = ["application/json"], method = [RequestMethod.POST])
    fun constructionPreprocess(@ApiParam(value = "", required = true) @RequestBody constructionPreprocessRequest: @Valid ConstructionPreprocessRequest?): ResponseEntity<ConstructionPreprocessResponse> {
        getRequest().ifPresent { request: NativeWebRequest ->
            for (mediaType in MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    val exampleString = "{ \"options\" : {} }"
                    ApiUtil.setExampleResponse(request, "application/json", exampleString)
                    break
                }
            }
        }
        return ResponseEntity(HttpStatus.valueOf(200))
    }


    @ApiOperation(value = "Get Metadata for Transaction Construction", nickname = "constructionMetadata", notes = "Get any information required to construct a transaction for a specific network. Metadata returned here could be a recent hash to use, an account sequence number, or even arbitrary chain state. The request used when calling this endpoint is often created by calling `/construction/preprocess` in an offline environment. It is important to clarify that this endpoint should not pre-construct any transactions for the client (this should happen in `/construction/payloads`). This endpoint is left purposely unstructured because of the wide scope of metadata that could be required.", response = ConstructionMetadataResponse::class, tags = ["Construction"])
    @ApiResponses(value = [ApiResponse(code = 200, message = "Expected response to a valid request", response = ConstructionMetadataResponse::class), ApiResponse(code = 200, message = "unexpected error", response = Error::class)])
    @RequestMapping(value = ["/construction/metadata"], produces = ["application/json"], consumes = ["application/json"], method = [RequestMethod.POST])
    fun constructionMetadata(@ApiParam(value = "", required = true) @RequestBody constructionMetadataRequest: @Valid ConstructionMetadataRequest?): ResponseEntity<ConstructionMetadataResponse> {
        if (getRequest().isPresent) {
            for (mediaType in MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
//                    val metadatas: Map<String, Any> = HashMap()
                    val metadatas = mutableMapOf<String,Any>()
                    val referenceBlockNum=chainXService.getMaxBlockNumber()
                    val block=chainXService.findBlockBean(referenceBlockNum.toInt()).block
                    val referenceBlockHash=block.curr_block_hash
                    val timestamp=block.time
                    val response = ConstructionMetadataResponse()
                    response.metadata = metadatas
                    metadatas.put("reference_block_num", referenceBlockNum)
                    metadatas.put("reference_block_hash", referenceBlockHash)
//                    metadatas.put("expiration", expiration)
                    metadatas.put("timestamp", timestamp)
                    return ResponseEntity(response, HttpStatus.OK)
                }
            }
        }
        return ResponseEntity(HttpStatus.valueOf(200))
    }


    @ApiOperation(value = "Generate an Unsigned Transaction and Signing Payloads", nickname = "constructionPayloads", notes = "Payloads is called with an array of operations and the response from `/construction/metadata`. It returns an unsigned transaction blob and a collection of payloads that must be signed by particular addresses using a certain SignatureType. The array of operations provided in transaction construction often times can not specify all \"effects\" of a transaction (consider invoked transactions in Ethereum). However, they can deterministically specify the \"intent\" of the transaction, which is sufficient for construction. For this reason, parsing the corresponding transaction in the Data API (when it lands on chain) will contain a superset of whatever operations were provided during construction.", response = ConstructionPayloadsResponse::class, tags = ["Construction"])
    @ApiResponses(value = [ApiResponse(code = 200, message = "Expected response to a valid request", response = ConstructionPayloadsResponse::class), ApiResponse(code = 200, message = "unexpected error", response = Error::class)])
    @RequestMapping(value = ["/construction/payloads"], produces = ["application/json"], consumes = ["application/json"], method = [RequestMethod.POST])
    fun constructionPayloads(
            @ApiParam(value = "", required = true) @RequestBody constructionPayloadsRequest: @Valid ConstructionPayloadsRequest?): ResponseEntity<ConstructionPayloadsResponse> {
        if (getRequest().isPresent) {
            for (mediaType in MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    val pair = buildTransaction(constructionPayloadsRequest)
                    val buildTransactionBean = pair.right
                    val owner = pair.left
                    val payloadItem = SigningPayload()
                    payloadItem.address(owner)
                            .hexBytes(buildTransactionBean.srcPubKey)
                            .signatureType(SignatureType.ECDSA_RECOVERY)
                    val response = ConstructionPayloadsResponse()
                    response.unsignedTransaction(buildTransactionBean.commonTransactionRaw)
                            .addPayloadsItem(payloadItem)
                    return ResponseEntity(response, HttpStatus.OK)
                }
            }
        }
        return ResponseEntity(HttpStatus.valueOf(200))
    }


    @ApiOperation(value = "Create Network Transaction from Signatures", nickname = "constructionCombine", notes = "Combine creates a network-specific transaction from an unsigned transaction and an array of provided signatures. The signed transaction returned from this method will be sent to the `/construction/submit` endpoint by the caller.", response = ConstructionCombineResponse::class, tags = ["Construction"])
    @ApiResponses(value = [ApiResponse(code = 200, message = "Expected response to a valid request", response = ConstructionCombineResponse::class), ApiResponse(code = 200, message = "unexpected error", response = Error::class)])
    @RequestMapping(value = ["/construction/combine"], produces = ["application/json"], consumes = ["application/json"], method = [RequestMethod.POST])
    fun constructionCombine(@ApiParam(value = "", required = true) @RequestBody constructionCombineRequest: @Valid ConstructionCombineRequest?): ResponseEntity<ConstructionCombineResponse> {
        val statusCode = AtomicInteger(HttpStatus.OK.value())
        getRequest().ifPresent { request: NativeWebRequest ->
            for (mediaType in MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    var returnString: String? = ""
                    try {
                        val constructionCombineResponse = ConstructionCombineResponse()
                        constructionCombineResponse.signedTransaction = constructionCombineRequest?.let { combineSerialize(it) }
                        returnString = objectMapper.writeValueAsString(constructionCombineResponse)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    ApiUtil.setExampleResponse(request, "application/json", returnString)
                    break
                }
            }
        }
        return ResponseEntity(HttpStatus.valueOf(statusCode.get()))
    }

    @ApiOperation(value = "Parse a Transaction", nickname = "constructionParse", notes = "Parse is called on both unsigned and signed transactions to understand the intent of the formulated transaction. This is run as a sanity check before signing (after `/construction/payloads`) and before broadcast (after `/construction/combine`). ", response = ConstructionParseResponse::class, tags = ["Construction"])
    @ApiResponses(value = [ApiResponse(code = 200, message = "Expected response to a valid request", response = ConstructionParseResponse::class), ApiResponse(code = 200, message = "unexpected error", response = Error::class)])
    @RequestMapping(value = ["/construction/parse"], produces = ["application/json"], consumes = ["application/json"], method = [RequestMethod.POST])
    fun constructionParse(@ApiParam(value = "", required = true) @RequestBody constructionParseRequest: @Valid ConstructionParseRequest?): ResponseEntity<ConstructionParseResponse> {
        val statusCode = AtomicInteger(200)
        getRequest().ifPresent { request: NativeWebRequest ->
            for (mediaType in MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    val returnString = ""
                    val mapper = ObjectMapper()
                    mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)
                    try {
                        val constructionParseResponse = ConstructionParseResponse()
                        val signed = constructionParseRequest!!.signed
                        val transaction = constructionParseRequest.transaction


                        //反序列transaction
                        val s = HashReader(Utils.HEX.decode(transaction))
                        val nVersion = s.readVarInt().value
                        val nTxType = s.readVarInt().value
                        val nValidHeight = s.readVarInt().value
                        val array = s.readUserId()
                        val userId = array[0]
                        val pubKey = array[1]
                        val feeSymbol = s.readString()
                        val fees = s.readVarInt().value
                        val dests = ArrayList<UCoinDest>()
                        s.readUCoinDestAddr(dests, instance)
                        val memo = s.readString()




                        val op_index: Long = 0
                    } catch (e: Exception) {
                    }
                    ApiUtil.setExampleResponse(request, "application/json", returnString)
                    break
                }
            }
        }
        return ResponseEntity(HttpStatus.valueOf(statusCode.get()))
    }


    @ApiOperation(value = "Get the Hash of a Signed Transaction", nickname = "constructionHash", notes = "TransactionHash returns the network-specific transaction hash for a signed transaction.", response = TransactionIdentifierResponse::class, tags = ["Construction"])
    @ApiResponses(value = [ApiResponse(code = 200, message = "Expected response to a valid request", response = TransactionIdentifierResponse::class), ApiResponse(code = 200, message = "unexpected error", response = Error::class)])
    @RequestMapping(value = ["/construction/hash"], produces = ["application/json"], consumes = ["application/json"], method = [RequestMethod.POST])
    fun constructionHash(@ApiParam(value = "", required = true) @RequestBody constructionHashRequest: @Valid ConstructionHashRequest?): ResponseEntity<TransactionIdentifierResponse> {
        val statusCode = AtomicInteger(HttpStatus.OK.value())
        getRequest().ifPresent { request: NativeWebRequest ->
            for (mediaType in MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    var returnString: String? = "{ \"transaction_hash\" : \"transaction_hash\" }"
                    try {
                        val transactionRaw = constructionHashRequest!!.signedTransaction
                        val transactionHash = Sha256Hash.hashTwice(Utils.HEX.decode(transactionRaw)).toString()
                        val transactionIdentifierResponse = TransactionIdentifierResponse()
                        transactionIdentifierResponse.transactionIdentifier = TransactionIdentifier().hash(transactionHash)
                        returnString = objectMapper.writeValueAsString(transactionIdentifierResponse)
                    } catch (e: Exception) {
                        e.printStackTrace()
                        statusCode.set(HttpStatus.INTERNAL_SERVER_ERROR.value())
                        val error = Constant.INVALID_TRANSACTION_FORMAT
                        try {
                            returnString = objectMapper.writeValueAsString(error)
                        } catch (ex: JsonProcessingException) {
                            //ex.printStackTrace();
                        }
                    }
                    ApiUtil.setExampleResponse(request, "application/json", returnString)
                    break
                }
            }
        }
        return ResponseEntity(HttpStatus.valueOf(statusCode.get()))
    }

    @ApiOperation(value = "Submit a Signed Transaction", nickname = "constructionSubmit", notes = "Submit a pre-signed transaction to the node. This call should not block on the transaction being included in a block. Rather, it should return immediately with an indication of whether or not the transaction was included in the mempool. The transaction submission response should only return a 200 status if the submitted transaction could be included in the mempool. Otherwise, it should return an error.", response = TransactionIdentifierResponse::class, tags = ["Construction"])
    @ApiResponses(value = [ApiResponse(code = 200, message = "Expected response to a valid request", response = TransactionIdentifierResponse::class), ApiResponse(code = 200, message = "unexpected error", response = Error::class)])
    @RequestMapping(value = ["/construction/submit"], produces = ["application/json"], consumes = ["application/json"], method = [RequestMethod.POST])
    fun constructionSubmit(
            @ApiParam(value = "", required = true) @RequestBody constructionSubmitRequest: @Valid ConstructionSubmitRequest?): ResponseEntity<TransactionIdentifierResponse> {
        val statusCode = AtomicInteger(HttpStatus.OK.value())
        getRequest().ifPresent { request: NativeWebRequest ->
            for (mediaType in MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    var returnString: String? = ""
                    try {
                        val transactionSigned = constructionSubmitRequest!!.signedTransaction


                        val txHash = chainXService.submitTxRaw(transactionSigned, null)
                        val transactionIdentifierResponse = TransactionIdentifierResponse()
                        val transactionIdentifier = TransactionIdentifier()
                        transactionIdentifier.hash = txHash
                        transactionIdentifierResponse.transactionIdentifier = transactionIdentifier
                        returnString = objectMapper.writeValueAsString(transactionIdentifierResponse)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    ApiUtil.setExampleResponse(request, "application/json", returnString)
                    break
                }
            }
        }
        return ResponseEntity(HttpStatus.valueOf(statusCode.get()))
    }

    private fun derivePubKey(hexBytes: String?): String {
        val pubKeyHash = Utils.sha256hash160(Utils.HEX.decode(hexBytes))
        val params = instance
        return LegacyAddress.fromPubKeyHash(params, pubKeyHash).toString()
    }

    private fun buildTransaction(constructionPayloadsRequest: ConstructionPayloadsRequest?): Pair<String, BuildTransactionBean> {
        val operations = constructionPayloadsRequest!!.operations
        val from: Operation
        val to: Operation
        if (StringUtils.isNotEmpty(operations[0].amount.value)
                && operations[0].amount.value.startsWith("-")) {
            from = operations[0]
            to = operations[1]
        } else {
            from = operations[1]
            to = operations[0]
        }
        val srcAddr = from.account.address
        val destAddr = to.account.address
        val amount = to.amount.value.toLong()
        val symbol = to.amount.currency.symbol
        val nValidHeight = chainXService.getMaxBlockNumber()
        val srcResult = chainXService.getAccountInfo(srcAddr)
        val destResult = chainXService.getAccountInfo(destAddr)
        val srcPubKey = srcResult!!.owner_pubkey
        val pubKey = Utils.HEX.decode(srcPubKey)
        val destPubKey = destResult!!.owner_pubkey
        val regId = srcResult.regid
        val memo = "transfer"

        val list = ArrayList<UCoinDest>()
        val uCoinDest = UCoinDest(LegacyAddress.fromPubKeyHash(instance, Utils.sha256hash160(Utils.HEX.decode(destPubKey))), symbol, amount)
        list.add(uCoinDest)

        val ss = HashWriter()

        ss.add(VarInt(1).encodeInOldWay())
                .add(VarInt(11).encodeInOldWay())
                .add(VarInt(nValidHeight).encodeInOldWay())
                .writeRegId(regId)
                .add(CoinType.WICC.type)
                .add(VarInt(100000L).encodeInOldWay())
                .addUCoinDestAddr(list)
                .add(memo)

       val hexStr = Utils.HEX.encode(ss.toByteArray())

        return Pair.of(srcAddr, BuildTransactionBean(srcPubKey, hexStr))
    }

    private fun combineSerialize(constructionCombineRequest: ConstructionCombineRequest):String{
        val unsignedTrx = constructionCombineRequest.unsignedTransaction

        //反序列unsignedTrx
        val s = HashReader(Utils.HEX.decode(unsignedTrx))
        val nVersion = s.readVarInt().value
        val nTxType = s.readVarInt().value
        val nValidHeight = s.readVarInt().value
        val regid  = s.readRegId()
        val feeSymbol = s.readString()
        val fees = s.readVarInt().value
        val dests = ArrayList<UCoinDest>()
        s.readUCoinDestAddr(dests, instance)
        val memo = s.readString()




        val ss = HashWriter()
        ss.add(VarInt(nTxType).encodeInOldWay())
                .add(VarInt(nVersion).encodeInOldWay())
                .add(VarInt(nValidHeight).encodeInOldWay())
                .writeRegId(regid)
                .add(feeSymbol)
                .add(VarInt(fees).encodeInOldWay())
                .addUCoinDestAddr(dests)
                .add(memo)

        //combine unsignedTrx and signatures
        val signature =  Utils.HEX.decode(constructionCombineRequest.signatures[0].hexBytes)
        ss.writeCompactSize(signature.size.toLong())
                .add(signature)

        return Utils.HEX.encode(ss.toByteArray())
    }



class TT{
    var prikey:String?=null
    var unsigntx:String?=null
}

    @PostMapping("/signtx")
    fun signtx(@RequestBody t: TT):ResponseEntity<String>{

        val mapper = ObjectMapper()
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)

        val params = WaykiTestNetParams.instance
        val key= DumpedPrivateKey.fromBase58(params, t.prikey).key
       // val key = ECKey.fromPrivate(Utils.HEX.decode(privateKey))
        val sigHash=Sha256Hash.hashTwice(Utils.HEX.decode(t.unsigntx))
        val ecSig=key.sign(Sha256Hash.wrap(sigHash))
        val signature = ecSig.encodeToDER()
        val signStr = Utils.HEX.encode(signature)

        return ResponseEntity(signStr, HttpStatus.OK)
    }

    @GetMapping("t/{hexStr}")
    fun tt(@PathVariable("hexStr") ss: String){
        val s = HashReader(Utils.HEX.decode(ss))
        val nVersion = s.readVarInt().value
        val nTxType = s.readVarInt().value
        val nValidHeight = s.readVarInt().value
        val regid  = s.readRegId()
        val feeSymbol = s.readString()
        val fees = s.readVarInt().value
        val dests = ArrayList<UCoinDest>()
        s.readUCoinDestAddr(dests, instance)
        val memo = s.readString()
        //val ret =  WaykiUCoinTxParams(nValidHeight, userId, pubKey, dests, feeSymbol, fees, memo)
        //ret.nVersion=nVersion
    }

}